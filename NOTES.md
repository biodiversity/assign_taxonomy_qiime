# assing_taxonomy.py qiime mod script
Here will provide a log on the customization of the `assingm_taxonomy.py` script

## preliminaries
1. install working enviromnet using miniconda project
```
conda create -n qiime1 python=2.7 qiime matplotlib=1.4.3 mock nose -c bioconda
```
2. active environment with
```
 source activate qiime1
```

3. install BLAST version 2.2.22 (other versions might not work properly)
``` 
    #if linux
    conda install -c bioconda blast-legacy=2.2.22
    #if macosx do a manual installation
    wget ftp.ncbi.nih.gov/blast/executables/legacy/2.2.22/blast-2.2.22-universal-macosx.tar.gz    
```

4. update enviromental valriable **BLASTMAT** in your PATH variable. Typical error `burrito.util.ApplicationNotFoundError: Cannot find formatdb. Is it installed? Is it in your path?`
```
   #if Linux
   export BLASTMAT="/home/kbessonov/my_soft/miniconda2/pkgs/blast-legacy-2.2.22-0/data"
   #if MacOS
   export BLASTMAT="/Users/kirill/mysoft/blast-2.2.22/data"
   export PATH="/Users/kirill/mysoft/blast-2.2.22/bin:$PATH"
```

5. once done you can deactivate environment with or just close terminal window
```
 source deactivate qiime1
```

## examples
Try running the OTU assignment with BLAST on 1 DNA sequences found in ```test_5otus.fasta```
```
  #simple search with blast using 1 sequence
  assign_taxonomy.py -i test_1otus.fasta -m blast
  #using a refernece sequences and taxonomy
  assign_taxonomy.py -i ./seqs/MiSeq_IndvdLeg.25K.fasta -r ./seqs/375_ref_new.fas -t ./seqs/375_ref_new.txt -m blast
```

The typical output using BLAST mode OTU identification method outputs 4 columns. These columns from left to right are
* id of the input sequence
* OTU name separated by a semicolon
* e-value (the lower the better)
* the nearest reference sequence

## execution details
Trigger the launch script is located at 
```
/home/kbessonov/my_soft/miniconda2/envs/qiime1/bin/assign_taxonomy.py
```

which calls the next script with the bulk code
```
/home/kbessonov/my_soft/miniconda2/envs/qiime1/lib/python2.7/site-packages/qiime-1.9.1-py2.7.egg-info/scripts/assign_taxonomy.py
```

2. From the class BlastTaxonAssigner the main data processing takes place (approx. line 450)
3. The main function which runs blast and selects the top hit is ```_seqs_to_taxonomy(self, seqs, blast_db, id_to_taxonomy_map)```. It accepts
* results object (i.e. self)
* query sequences (i.e. seqs)
* temporary path to blast_db (i.e. blast_db)
* dictionary of seq IDs to taxonomy names with maps (i.e. id_to_taxonomy_map)

## side notes
`uclust` is the default method
```
   uclast --input <*.fasta>  --id 0.9
 ```
 
# all script locations
```
./my_soft/miniconda2/envs/qiime1/bin/assign_taxonomy.py
./my_soft/miniconda2/envs/qiime1/lib/python2.7/site-packages/qiime-1.9.1-py2.7.egg-info/scripts/assign_taxonomy.py
./my_soft/miniconda2/envs/qiime1/lib/python2.7/site-packages/qiime/assign_taxonomy.py
./my_soft/miniconda2/envs/qiime1/lib/python2.7/site-packages/qiime/parallel/assign_taxonomy.py
./my_soft/miniconda2/pkgs/qiime-1.9.1-py27_0/bin/assign_taxonomy.py
./my_soft/miniconda2/pkgs/qiime-1.9.1-py27_0/lib/python2.7/site-packages/qiime-1.9.1-py2.7.egg-info/scripts/assign_taxonomy.py
./my_soft/miniconda2/pkgs/qiime-1.9.1-py27_0/lib/python2.7/site-packages/qiime/assign_taxonomy.py
./my_soft/miniconda2/pkgs/qiime-1.9.1-py27_0/lib/python2.7/site-packages/qiime/parallel/assign_taxonomy.py
```
