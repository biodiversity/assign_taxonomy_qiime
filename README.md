# INSTALL
* install MiniConda as per [Qiime instructions](http://qiime.org/install/install.html)
``` 
   source activate qiime1
```
* backup original `assign_taxonomy.py` script (optional)
```
  cd <path-to-folder>/miniconda/envs/qiime1/lib/python2.7/site-packages/qiime/assign_taxonomy.py
  cp assign_taxonomy.py assign_taxonomy.py.bkp
```
note: `<path-to-folder>` is the path to `miniconda` installation folder
* copy the modified `assign_taxonomy.py` to library directory replacing original one. 
Note that in newer releases the master script `/usr/bin/assign_taxonomy.py` loads the `assign_taxonomy.py` stored at `/usr/lib/python2.7/site-packages/qiime/assign_taxonomy.py` 
```
  cp assign_taxonomy.py <path-to-folder>/miniconda/envs/qiime1/lib/python2.7/site-packages/qiime/
```
* define the global variable BLASTMAT (blast matrix)
```
   export BLASTMAT=/opt/blast-2-2.22/data
```
* run your analysis supplying relative paths to required files
```
     assign_taxonomy.py -i <query.fasta> -r <refereces.fasta> -t <references.txt> -m blast
     # run example
     assign_taxonomy.py -i ./seqs/MiSeq_IndvdLeg.25K.fasta -r ./seqs/375_ref_new.fas -t ./seqs/375_ref_new.txt -m blast
```
* check for results in the `blast_assigned_taxonomy` folder
* typical ouput of the results.txt file
```
   SampleSeqID     Lineage e-value %Identity       RefSeqID
OTU_1278;size=1;        Arthropoda;Insecta;Lepidoptera;Crambidae;Scoparia;Scoparia biplagialis  6e-169  94.55   RRGCO629-15
```